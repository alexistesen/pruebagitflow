package com.everis.core;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everis.core.entity.Student;
import com.everis.core.repository.StudentRepository;

@RestController
public class StudentController {
	
	
	@Autowired
	private StudentRepository repository;
	
	@RequestMapping(value="/api/student/list", method = RequestMethod.GET)
	public ResponseEntity<Student> list() {
		List<Student> list = new ArrayList<Student>();
		repository.findAll().forEach(list::add);
		return new ResponseEntity(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/student/delete",method=RequestMethod.DELETE)
	public ResponseEntity<Student> remove(@RequestParam(value="id") long id){
		repository.deleteById(id);
		return new ResponseEntity<Student>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/student/create", method = RequestMethod.POST)
	public ResponseEntity<Student> create(@Valid @RequestBody Student student) {
		return new ResponseEntity(repository.save(student),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/api/student/find", method = RequestMethod.GET)
	public ResponseEntity<Student> userById(@RequestParam(value="id") long id) {
		return new ResponseEntity(repository.findById(id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/student/update", method = RequestMethod.PATCH)
	public ResponseEntity<Student> update(@RequestParam(value="id") long id, @Valid @RequestBody Student student) {
		Student buscado = repository.findById(id).get();
		buscado.setDate_of_birth(student.getDate_of_birth());
		buscado.setFirst_name(student.getFirst_name());
		buscado.setGender(student.getGender());
		buscado.setLast_name(student.getLast_name());
		buscado.setMiddle_name(student.getMiddle_name());
		buscado.setOther_student_details(student.getOther_student_details());
		return new ResponseEntity(repository.save(buscado),HttpStatus.CREATED);
	}
	
}
