package com.everis.core;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everis.core.entity.Parent;
import com.everis.core.repository.ParentRepository;

@RestController
public class ParentController {

	@Autowired
	private ParentRepository repository;
	
	@RequestMapping(value="/api/parent/list", method = RequestMethod.GET)
	public ResponseEntity<Parent> list() {
		List<Parent> list = new ArrayList<Parent>();
		repository.findAll().forEach(list::add);
		return new ResponseEntity(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/parent/remove",method = RequestMethod.DELETE)
	public ResponseEntity<Parent> remove(@RequestParam(value="id") long id){
		repository.deleteById(id);
		return new ResponseEntity<Parent>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/parent/create", method = RequestMethod.POST)
	public ResponseEntity<Parent> create(@Valid @RequestBody Parent parent) {
		return new ResponseEntity(repository.save(parent),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/api/parent/find", method = RequestMethod.GET)
	public ResponseEntity<Parent> userById(@RequestParam(value="id") long id) {
		return new ResponseEntity(repository.findById(id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/parent/update", method = RequestMethod.PATCH)
	public ResponseEntity<Parent> update(@RequestParam(value="id") long id, @Valid @RequestBody Parent parent) {
		Parent buscado = repository.findById(id).get();
		buscado.setFirst_name(parent.getFirst_name());
		buscado.setGender(parent.getGender());
		buscado.setLast_name(parent.getLast_name());
		buscado.setMiddle_name(parent.getMiddle_name());
		buscado.setOther_parent_details(parent.getOther_parent_details());
		return new ResponseEntity(repository.save(buscado),HttpStatus.CREATED);
	}
	
}
