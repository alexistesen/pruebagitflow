package com.everis.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

// ** AUN NO ESTOY SEGURO DE ESTA TABLA
@Entity
public class Family_Members implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8773481264027778842L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long family_member_id;
	
	@JoinColumn(name="family_id")
	@ManyToOne
	private Families family_id;
	
	@Column
	private String parent_of_student_member;
	
	@JoinColumn(name="parent_id")
	@ManyToOne
	private Parent parent_id;
	
	@JoinColumn(name="student_id")
	@ManyToOne
	private Student student_id;

	public Long getFamily_member_id() {
		return family_member_id;
	}

	public void setFamily_member_id(Long family_member_id) {
		this.family_member_id = family_member_id;
	}

	public Families getFamily_id() {
		return family_id;
	}

	public void setFamily_id(Families family_id) {
		this.family_id = family_id;
	}

	public String getParent_of_student_member() {
		return parent_of_student_member;
	}

	public void setParent_of_student_member(String parent_of_student_member) {
		this.parent_of_student_member = parent_of_student_member;
	}

	public Parent getParent_id() {
		return parent_id;
	}

	public void setParent_id(Parent parent_id) {
		this.parent_id = parent_id;
	}

	public Student getStudent_id() {
		return student_id;
	}

	public void setStudent_id(Student student_id) {
		this.student_id = student_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((family_member_id == null) ? 0 : family_member_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Family_Members other = (Family_Members) obj;
		if (family_member_id == null) {
			if (other.family_member_id != null)
				return false;
		} else if (!family_member_id.equals(other.family_member_id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Family_Members [family_member_id=" + family_member_id + ", family_id=" + family_id
				+ ", parent_of_student_member=" + parent_of_student_member + ", parent_id=" + parent_id
				+ ", student_id=" + student_id + "]";
	}
	
}
