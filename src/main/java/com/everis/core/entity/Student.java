package com.everis.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Student implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2664602977431863231L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long student_id;
	
	@Column
	@JsonProperty("Gender")
	@NotBlank
	private String gender;
	
	@Column
	@JsonProperty("FirstName")
	@NotBlank
	private String first_name;
	
	@Column
	@JsonProperty("MiddleName")
	@NotBlank
	private String middle_name;
	
	@Column
	@JsonProperty("LastName")
	@NotBlank
	private String last_name;
	
	@Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@Past
	private Date date_of_birth;
	
	@Column
	@JsonProperty("Info")
	private String other_student_details;
	
	@ManyToMany
	@JoinTable(name="Students_Parents",joinColumns=@JoinColumn(name="student_id"),inverseJoinColumns=@JoinColumn(name="parent_id"))
	private Set<Parent> parent_id;


	public Set<Parent> getParent_id() {
		return parent_id;
	}

	public void setParent_id(Set<Parent> parent_id) {
		this.parent_id = parent_id;
	}

	public Long getStudent_id() {
		return student_id;
	}

	public void setStudent_id(Long student_id) {
		this.student_id = student_id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Date getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getOther_student_details() {
		return other_student_details;
	}

	public void setOther_student_details(String other_student_details) {
		this.other_student_details = other_student_details;
	}

	@Override
	public String toString() {
		return "Student [student_id=" + student_id + ", gender=" + gender + ", first_name=" + first_name
				+ ", middle_name=" + middle_name + ", last_name=" + last_name + ", date_of_birth=" + date_of_birth
				+ ", other_student_details=" + other_student_details + ", parent_id=" + parent_id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((student_id == null) ? 0 : student_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (student_id == null) {
			if (other.student_id != null)
				return false;
		} else if (!student_id.equals(other.student_id))
			return false;
		return true;
	}
	
	
}
