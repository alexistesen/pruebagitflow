package com.everis.core;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everis.core.entity.Family_Members;
import com.everis.core.repository.Family_MembersRepository;

@RestController
public class Family_MembersController {

	@Autowired
	Family_MembersRepository repository;
	
	@RequestMapping(value="/api/members/list", method = RequestMethod.GET)
	public ResponseEntity<Family_Members> list() {
		List<Family_Members> list = new ArrayList<Family_Members>();
		repository.findAll().forEach(list::add);
		return new ResponseEntity(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/members/remove",method = RequestMethod.DELETE)
	public ResponseEntity<Family_Members> remove(@RequestParam(value="id") long id){
		repository.deleteById(id);
		return new ResponseEntity<Family_Members>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/members/create", method = RequestMethod.POST)
	public ResponseEntity<Family_Members> create(@Valid @RequestBody Family_Members members) {
		return new ResponseEntity(repository.save(members),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/api/members/find", method = RequestMethod.GET)
	public ResponseEntity<Family_Members> userById(@RequestParam(value="id") long id) {
		return new ResponseEntity(repository.findById(id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/members/update", method = RequestMethod.PATCH)
	public ResponseEntity<Family_Members> update(@RequestParam(value="id") long id, @Valid @RequestBody Family_Members members) {
		Family_Members buscado = repository.findById(id).get();
		buscado.setFamily_id(members.getFamily_id());
		buscado.setParent_of_student_member(members.getParent_of_student_member());
		buscado.setStudent_id(members.getStudent_id());
		buscado.setParent_id(members.getParent_id());
		return new ResponseEntity(repository.save(buscado),HttpStatus.CREATED);
	}
}
