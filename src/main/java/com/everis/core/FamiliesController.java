package com.everis.core;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everis.core.entity.Families;
import com.everis.core.repository.FamiliesRepository;

@RestController
public class FamiliesController {

	@Autowired
	FamiliesRepository repository;
	
	@RequestMapping(value="/api/families/list", method = RequestMethod.GET)
	public ResponseEntity<Families> list() {
		List<Families> list = new ArrayList<Families>();
		repository.findAll().forEach(list::add);
		return new ResponseEntity(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/families/remove",method = RequestMethod.DELETE)
	public ResponseEntity<Families> remove(@RequestParam(value="id") long id){
		repository.deleteById(id);
		return new ResponseEntity<Families>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/families/create", method = RequestMethod.POST)
	public ResponseEntity<Families> create(@Valid @RequestBody Families families) {
		return new ResponseEntity(repository.save(families),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/api/families/find", method = RequestMethod.GET)
	public ResponseEntity<Families> userById(@RequestParam(value="id") long id) {
		return new ResponseEntity(repository.findById(id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/families/update", method = RequestMethod.PATCH)
	public ResponseEntity<Families> update(@RequestParam(value="id") long id, @Valid @RequestBody Families families) {
		Families buscado = repository.findById(id).get();
		buscado.setFamily_name(families.getFamily_name());
		buscado.setHead_of_family_parent_id(families.getHead_of_family_parent_id());
		return new ResponseEntity(repository.save(buscado),HttpStatus.CREATED);
	}
	
	
}
