package com.everis.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.everis.core.entity.Families;

public interface FamiliesRepository extends JpaRepository<Families,Long>{

}
