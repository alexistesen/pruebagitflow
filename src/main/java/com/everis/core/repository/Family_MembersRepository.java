package com.everis.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.everis.core.entity.Family_Members;

public interface Family_MembersRepository extends JpaRepository<Family_Members,Long>{

}
