package com.everis.core.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.everis.core.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long>	 {

}
