package com.everis.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.everis.core.entity.Parent;

public interface ParentRepository extends JpaRepository<Parent,Long> {

}
